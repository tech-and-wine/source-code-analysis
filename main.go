package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/parakeet-nest/parakeet/completion"
	"github.com/parakeet-nest/parakeet/llm"
)

func main() {

	ollamaUrl := "http://localhost:11434"
	//model := "deepseek-coder" // 😡
	//model := "llama3"
	//model := "mistral:latest"
	//model := "phi3:medium" // 🙂
	model := "phi3:mini" // 🙂
	//model := "granite-code:3b" // 😡
	//model := "codeqwen:latest" // 🙂
	//model := "codegemma"
	//model := "deepseek-coder:6.7b" // 😡
	//model := "starcoder:1b" // 😡

	systemContent := `You are an helpful developer expert. 
	You know how to read code. 
	Explain what the provided source code by the user is doing.
	And give some advice to improve it.
	`

	// it would be better to give an example of job
	var contextContent = `<context>
	</context>
	`
	// TODO:
	// - Use the GitLab API to get the code from the project
	// - Or use it in a pipeline and generate a report

	sourceCode, err := os.ReadFile("./equation.js")

	if err != nil {
		log.Fatal("😡:", err)
	}

	userContent := string(sourceCode)

	options := llm.Options{
		Temperature:   0.5, // default (0.8)
		RepeatLastN:   2,   // default (64) the default value will "freeze" deepseek-coder
		RepeatPenalty: 1.5,
	}

	query := llm.Query{
		Model: model,
		Messages: []llm.Message{
			{Role: "system", Content: systemContent},
			{Role: "system", Content: contextContent},
			{Role: "user", Content: userContent},
		},
		Options: options,
	}

	answer, chatErr := completion.ChatStream(ollamaUrl, query,
		func(answer llm.Answer) error {
			fmt.Print(answer.Message.Content)
			return nil
		})

	if chatErr != nil {
		log.Fatal("😡:", chatErr)
	}

	fileName := "result-" + strings.ReplaceAll(model, ":", "_") + ".md"
	_ = os.WriteFile(fileName, []byte(answer.Message.Content), 0644)

}
